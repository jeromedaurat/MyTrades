<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->unsignedBigInteger('balance_id')->nullable(true)->default(null);
            $table->unsignedBigInteger('pair_id')->nullable(true)->default(null);
            $table->foreign('pair_id')->references('id')->on('pairs');
            $table->foreign('balance_id')->references('id')->on('balances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->removeColumn('balance_id');
            $table->removeColumn('pair_id');
            $table->dropForeign('balance_id');
            $table->dropForeign('pair_id');
        });
    }
}
