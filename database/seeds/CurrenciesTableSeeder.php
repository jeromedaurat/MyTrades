<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = ['btc', 'eth', 'ltc', 'usd', 'usdt', 'bnb'];
        foreach ($currencies as $currency) {
            DB::table('currencies')->insert([
                'name' => $currency,
            ]);
        }
    }
}
