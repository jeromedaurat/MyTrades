<?php

use Illuminate\Database\Seeder;
use App\Models\Exchange;

class ExchangesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exchanges = ['Binance', 'Bitfinex', 'Bitmex', 'Bittrex', 'Coinbase'];
        /* 'HitBTC', 'Kraken', 'KuCoin', 'YoBit' */
        foreach ($exchanges as $exchange) {
            $exchangeObject = Exchange::firstOrCreate([
                'name' => $exchange,
            ]);
            $exchangeObject->initExchange();
        }
    }
}
