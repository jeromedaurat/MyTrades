<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'ProfileController@index')->name('profile');

    // Exchange
    Route::resource('exchange', 'ExchangeController');
    Route::resource('account', 'AccountController');
    // Send Mail
    Route::get('test', 'TradeController@update');
    /* Route::group(['prefix' => 'account'], function () {
        Route::get('create', 'AccountController@new')->name('account.create');
        Route::post('/', 'AccountController@store')->name('account.store');
        Route::get('{id}', 'AccountController@new')->name('account.show');
        Route::put('{id}', 'AccountController@update')->name('account.update');
        Route::delete('{id}', 'AccountController@new')->name('account.destroy');
        Route::fallback('AccountController@fallback');
    }); */
});
