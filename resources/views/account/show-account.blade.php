@extends('layouts.app')

@section('content')
<div class="container">
    <h3>{{ $account->exchange->name }} {{ __('Account') }}</h3>
    @if (count($errors))
    <div class="card mb-3">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    @endif

    <div class="card mb-3">
        <div class="card-header">
            {{ __('Actions') }}
        </div>
        <div class="card-body">
            <p><img width="85" height="25" src="{{asset('images/exchanges/'.strtolower($account->exchange->name).'.jpg')}}"/> This account has been created {!! $account->last_use !!}
            <div class="form-group">
                <form action="{{route('account.destroy', ['id' => $account->id])}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger" value="1">{{ __('Delete this account') }}</a>
                </form>
            </div>
            <div class="form-group">
                <form action="{{route('account.update', ['id' => $account->id])}}" method="POST">
                    @csrf
                    @method('PUT')
                    @if ($account->auto_refresh)
                        <button type="submit" class="btn btn-warning" name="auto_refresh" value="0">{{ __('Disable auto-refresh') }}</a>
                    @else
                        <button type="submit" class="btn btn-primary" name="auto_refresh" value="1">{{ __('Enable auto-refresh') }}</a>
                    @endif
                </form>
            </div>
        </div>
    </div>

    <div class="card mb-3">
        <div class="card-header">
            {{ __('Balance') }}
        </div>
            @if (count($account->balances))
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">@sortablelink('currency.name', 'Name')</th>
                            <th scope="col">@sortablelink('quantity', 'Quantity', ['account_id' => 1])</th>
                            <th scope="col">BTC Value</th>
                            <th scope="col">USD Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($balances as $balance)
                            <tr>
                                <th scope="row">{{$balance->currency->name}}</th>
                                <th scope="row">{{number_format($balance->quantity, 2, ',', '')}}</th>
                                <th scope="row">{{ number_format($balance->getBitcoinPriceEquivalent(), 8, ',', '')}}</th>
                                <th scope="row">{{ number_format($balance->getUsdtPriceEquivalent(), 2, ',', '')}}</th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $balances->appends(\Request::except('page'))->render() !!}
            @else
                <div class="alert alert-info">
                    Account has no balance
                </div>
            @endif
    </div>
</div>
@endsection