@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card">
		<div class="card-header">
			{{ __('Link your exchange\'s account') }}
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div><br />
		@endif
		<div class="card-body">
		<form method="post" action="{{ route('account.store') }}">
		  <div class="form-group">
				@csrf
				<label for="exchange_id">{{ __('Exchange') }}</label>
				<select class="form-control" name="exchange_id">
					<option>{{ __('Select Exchange') }}</option>
					@foreach ($exchanges as $exchange)
						<option value="{{ $exchange->id }}"> 
							{{ $exchange->name }} 
						</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
			  <label for="name">{{ __('Name') }}</label>
				<input type="text" class="form-control" name="name"/>
				<small id="nameHelp" class="form-text text-muted">The name used to identify this exchange account. Whatever you want</small>
		  </div>
			<br />
		  <div class="alert alert-warning" role="alert">
			{!! __('<strong>Do not</strong> put public and private keys that have withdrawal and/or trading authorization') !!}
			</div>
		  <div class="form-group">
			  <label for="public_key">{{ __('Public Key') }}</label>
			  <input type="text" class="form-control" name="public_key"/>
		  </div>
		  <div class="form-group">
			  <label for="private_key">{{ __('Private Key') }}</label>
			  <input type="text" class="form-control" name="private_key"/>
		  </div>
		  <button type="submit" class="btn btn-primary">{{ __('Add Account') }}</button>
		</form>
		</div>
	</div>
</div>
@endsection