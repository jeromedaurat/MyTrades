@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card">
		<div class="card-header">
			{{ __('Create balance') }}
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div><br />
		@endif
		<div class="card-body">
		<form method="post" action="{{ route('account.store') }}">
		  <div class="form-group">
			@csrf
			
          </div>
          <br />
		  <div class="form-group">
			  <label for="quantity">{{ __('quantity') }}</label>
			  <input type="quantity" class="form-control" name="quantity"/>
		  </div>
		  <button type="submit" class="btn btn-primary">{{ __('Add Balance') }}</button>
		</form>
		</div>
	</div>
</div>
@endsection