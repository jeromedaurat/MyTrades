@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            {{ __('Exchanges') }}
        </div>
        @foreach ($exchanges as $exchange)
            <p>{{$exchange->name}} ({{count($exchange->pairs)}})</p>
        @endforeach
    </div>
</div>
@endsection