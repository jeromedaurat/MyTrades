@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            {{ __('Create an exchange') }}
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
            </div><br />
        @endif
        <div class="card-body">
        <form method="post" action="{{ route('exchange.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">{{ __('Exchange Name') }}</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <button type="submit" class="btn btn-primary">{{ __('Add Exchange') }}</button>
        </form>
        </div>
    </div>
</div>
@endsection