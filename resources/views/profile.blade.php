@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ __('My Profile') }}</div>
        <div class="card-body">
            <div class="container">
                <h2>{{ __('My Informations') }}</h2>
                <div class="row justify-content-md-center">
                    <div class="col col">
                    {{ __('Name') }}
                    </div>
                    <div class="col col">
                        <b>{{ $user->name }}</b>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col col">
                    {{ __('Email') }}
                    </div>
                    <div class="col col">
                        <b>{{ $user->email }}</b>
                    </div>
                </div>
            </div>
        </div>
    </div><br />

    <div class="well">
    <h3>{{ __('My Accounts') }} <a class="btn btn-primary" href="{{ route('account.create') }}"> Add Account <i class="fas fa-user-plus"></i></a></h3>
        <div class="card-deck">
            @foreach ($user->accounts as $account)
            <div class="card md-6 text-center">
                <div class="card-header">
                    {{ $account->name }}
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ __($account->exchange->name.'\'s account') }}</h5>
                    <p class="card-text">This account has been used for {{ count($account->trades) }} trades. </p>
                    <p class="card-text"><small class="text-muted">{{ __('Created the') }} {{ date('d/m/Y', strtotime($account->created_at)) }}</small></p>
                        <a href="{{ route('account.show', ['account' => $account])}}" class="btn btn-primary">{{ __('See more') }}</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection