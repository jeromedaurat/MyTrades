<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'mytrades');

// Project repository
set('repository', 'git@gitlab.com:jeromedaurat/MyTrades.git');

// Shared files/dirs between deploys 
set('shared_files', [
  '.env'
]);
set('shared_dirs', [
  'storage/app/public'
]);

set('git_recursive', false);
set('git_cache', false);


// Writable dirs by web server 
add('writable_dirs', ['.']);
set('writable_dirs_recursive', true);
set('writable_mode', 'chown');
set('writable_use_sudo', true);
set('clear_use_sudo', true);
set('cleanup_use_sudo', true);

set('http_user', 'www-data');
set('allow_anonymous_stats', false);

// Hosts

localhost('localhost')
    ->set('deploy_path', '/home/www/{{application}}.jeromedaurat.com')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

