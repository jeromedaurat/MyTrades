<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    public function pair()
    {
        return $this->belongsTo('\App\Models\Pair');
    }

    public function account()
    {
        return $this->belongsTo('\App\Models\Account');
    }
}
