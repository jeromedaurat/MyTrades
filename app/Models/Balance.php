<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Balance extends Model
{
    use Sortable;

    protected $fillable = [
        'quantity',
        'account_id',
        'currency_id',
    ];

    function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    function account()
    {
        return $this->belongsTo('App\Models\Account');
    }

    function getBitcoinPriceEquivalent()
    {
        $pair = Pair::where([
            'currency_name_id' => $this->currency->id,
            'currency_base_id' => Currency::where(['name' => 'BTC'])->first()->id,
            'exchange_id' => $this->account->exchange->id
        ])->first();

        if (isset($pair->id)) {
            if ($price_log = PriceLog::where('pair_id', $pair->id)->orderBy('created_at', 'desc')->first()) {
                return $price_log->price * $this->quantity;
            }
        }
        return 0;
    }

    function getUsdtPriceEquivalent()
    {
        $pair = Pair::where([
            'currency_name_id' => Currency::where(['name' => 'BTC'])->orderBy('created_at', 'desc')->first()->id,
            'currency_base_id' => Currency::where(['name' => 'USDT'])->orderBy('created_at', 'desc')->first()->id,
            'exchange_id' => $this->account->exchange->id
        ])->first();

        if (isset($pair->id)) {
            if ($price_log = PriceLog::where('pair_id', $pair->id)->orderBy('created_at', 'desc')->first()) {
                return $price_log->price * $this->getBitcoinPriceEquivalent();
            }
        }
        return 0;
    }
}
