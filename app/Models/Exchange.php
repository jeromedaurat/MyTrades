<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $fillable = [
        'name',
    ];

    public static function createExchange(string $exchange_name, array $options)
    {
        $exchange_class = '\ccxt\\' . $exchange_name;
        if (class_exists($exchange_class)) {
            return new $exchange_class($options);
        }
    }

    public function initExchange()
    {
        $exchange_ccxt = self::createExchange($this->name, []);
        echo "Getting exchange's pairs for $this->name" . "\n";
        foreach ($exchange_ccxt->load_markets() as $pair) {
            $name = Currency::firstOrCreate([
                'name' => $pair['base']
            ]);
            $base = Currency::firstOrCreate([
                'name' => $pair['quote']
            ]);
            Pair::firstOrCreate([
                'currency_name_id' => $name->id,
                'currency_base_id' => $base->id,
                'exchange_id' => $this->id,
            ]);
        }
    }

    public static function getBalances()
    {
        $accounts = DB::table('accounts')->orderBy('id')->where('auto_refresh', '1')->get();
        $this->line('Syncing balances...');
        $bar = $this->output->createProgressBar(count($accounts));

        $bar->start();

        foreach ($accounts as $account) {
            $this->performTask($account);

            $bar->advance();
        }

        $bar->finish();
    }

    function pairs()
    {
        return $this->hasMany('App\Models\Pair');
    }
}
