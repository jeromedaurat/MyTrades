<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriceLog extends Model
{
    protected $fillable = [
        'pair_id',
        'price'
    ];

    public function pair()
    {
        return $this->belongsTo('\App\Models\Pair');
    }
}
