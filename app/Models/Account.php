<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class Account extends Model
{

    use Sortable;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'exchange_id',
        'private_key',
        'public_key',
    ];

    protected $hidden = [
        'private_key',
    ];

    function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    function exchange()
    {
        return $this->belongsTo('App\Models\Exchange');
    }

    function trades()
    {
        return $this->hasMany('App\Models\Trade');
    }

    function balances()
    {
        return $this->hasMany('App\Models\Balance');
        //return Balance::where(['account_id' => $this->id])->get();
    }

    public function getTokens(): array
    {
        return [
            'apiKey' => $this->public_key,
            'secret' => $this->private_key,
        ];
    }

    public function getLastUse()
    {
        return $this->created_at->diffForHumans();
    }
}
