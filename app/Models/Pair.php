<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pair extends Model
{
    protected $table = 'pairs';

    protected $fillable = [
        'currency_name_id',
        'currency_base_id',
        'exchange_id',
    ];

    public $timestamps = false;

    function exchange()
    {
        return $this->belongsTo('App\Models\Exchange');
    }

    function name()
    {
        return $this->hasOne('App\Models\Currency', 'currency_name_id');
    }

    function base()
    {
        return $this->hasOne('App\Models\Currency', 'currency_base_id');
    }

    function price_logs()
    {
        return $this->hasMany('\App\Models\PriceLog');
    }
}
