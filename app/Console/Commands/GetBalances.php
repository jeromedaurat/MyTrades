<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\Exchange;
use App\Models\Currency;
use App\Models\Balance;

class GetBalances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccxt:balances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch the balances from every exchanges';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = DB::table('accounts')->orderBy('id')->where('auto_refresh', '1')->get();
        if (!count($accounts)) {
            $this->error('No accounts to refresh.');
        } else {
            $this->line('Syncing balances...');
            $bar = $this->output->createProgressBar(count($accounts));

            $bar->start();

            foreach ($accounts as $account) {
                $this->performTask($account);
                $bar->advance();
            }
            $bar->finish();
        }
    }

    public function performTask($account): bool
    {
        $exchange = Exchange::createExchange(strtolower(Exchange::find($account->exchange_id)->name), [
            'apiKey' => $account->public_key,
            'secret' => $account->private_key,
        ]);
        try {
            Log::channel('daily')->info('Fetching balance for account #' . $account->id);
            $balance_raw = $exchange->fetchBalance();
        } catch (\ccxt\NetworkError $e) {
            $this->error("\n[ERROR] NetworkError: fetch_balance failed : " . $e->getMessage());
            Log::channel('daily')->error('NetworkError: fetch_balance failed : ' . $e->getMessage());
            return false;
        } catch (\ccxt\ExchangeError $e) {
            $this->error("\n[ERROR] ExchangeError: fetch_balance failed : " . $e->getMessage());
            Log::channel('daily')->error('ExchangeError: fetch_balance failed : ' . $e->getMessage());
            return false;
        } catch (Exception $e) {
            $this->error("\n[ERROR] Unhandled exception: fetch_balance failed : " . $e->getMessage());
            Log::channel('daily')->error('Unhandled exception: fetch_balance failed : ' . $e->getMessage());
            return false;
        }

        foreach ($balance_raw['info']['balances'] as $balance) {
            $balanceObject = Balance::firstOrCreate([
                'account_id' => $account->id,
                'currency_id' => Currency::firstOrCreate(['name' => $balance['asset']])->id,
            ]);
            $balanceObject->quantity = (double)$balance['free'] + (double)$balance['locked'];
            $balanceObject->save();
        }
        return true;
    }
}
