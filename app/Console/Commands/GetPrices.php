<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Exchange;
use App\Models\PriceLog;
use App\Models\Currency;

class GetPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccxt:prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch the prices of every coins from every exchanges';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get Every Exchanges linked to a 'auto-refresh' Account
        // For each exchanges get all the tickers prices from the accounts.

        /*
         * SELECT `a`.`exchange_id`, GROUP_CONCAT(c.name) as currencies
         * FROM currencies c
         * JOIN balances b ON (b.currency_id = c.id AND b.quantity > 0)
         * JOIN accounts a ON (a.id = b.account_id AND a.auto_refresh = 1)
         * GROUP BY `a`.`exchange_id`
         **/
        $exchanges = DB::table('currencies')
            ->join('balances', function ($join) {
                $join->on('balances.currency_id', '=', 'currencies.id')
                    ->where('balances.quantity', '>', '0');
            })
            ->join('accounts', function ($join) {
                $join->on('accounts.id', '=', 'balances.account_id')
                    ->where('accounts.auto_refresh', '=', '1');
            })
            ->select(['accounts.exchange_id', DB::raw('GROUP_CONCAT(currencies.name) as currencies')])
            ->groupBy('accounts.exchange_id')
            ->get();

        if (!count($exchanges)) {
            $this->error('No tickers to get.');
        } else {
            $this->line('Getting tickers...');
            $bar = $this->output->createProgressBar(count($exchanges));

            $bar->start();

            foreach ($exchanges as $exchange) {
                if (!$this->performTask($exchange)) {
                    return;
                }
                $bar->advance();
            }
            $bar->finish();
        }
    }

    public function performTask($exchange_currencies): bool
    {
        $exchange = Exchange::find($exchange_currencies->exchange_id);

        $exchange_ccxt = Exchange::createExchange(strtolower($exchange->name), []);
        try {
            foreach (explode(',', $exchange_currencies->currencies) as $currency_name) {
                $currency_id = Currency::where(['name' => $currency_name])->first()->id;
                foreach (DB::table('pairs')
                    ->where([
                        ['currency_name_id', $currency_id],
                        ['exchange_id', $exchange->id],
                    ])
                    ->get() as $pair) {
                    $balance_raw = $exchange_ccxt->fetchTicker(
                        Currency::where('id', $pair->currency_name_id)->first()->name . '/' . Currency::where('id', $pair->currency_base_id)->first()->name
                    );
                    PriceLog::create([
                        'pair_id' => $pair->id,
                        'price' => $balance_raw['last'],
                    ]);
                }
            }
            return true;
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return false;
        }
    }
}
