<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Pair;
use App\Models\Balance;
use App\Models\Exchange;
use Illuminate\Http\Request;
use \Validator;
use Illuminate\Support\Facades\Auth;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Support\Facades\DB;



class AccountController extends Controller
{

    public function index()
    {
        return redirect()->route('profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('account/create-account', ['exchanges' => Exchange::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'exchange_id' => 'required|integer|bail',
            'public_key' => 'required|string|max:100',
            'private_key' => 'required|string|max:100',
        ];

        $messages = [
            'exchange_id.*' => 'You must choose an exchange',
            'exchange_id' => 'You must choose an exchange',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('account.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();
        $data['user_id'] = auth()->id();
        $account = new Account;
        $account->public_key = $data['public_key'];
        $account->private_key = $data['private_key'];
        $account->exchange_id = Exchange::findOrFail($data['exchange_id'])->id;
        $account->user_id = Auth::id();
        $account->save();
        return redirect()->route('profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        $errors = [];
        $trades = [];
        $balances_array = [];
        /* foreach ($account->balances as $balance) {
            $balances_array[] = [
                'quantity' => $balance->quantity,
                'currency' => DB::table('currencies')->where(['id' => $balance->currency_id])->first()->name
            ];
        } */
        /* var_dump(Redis::get('key'));
        die; */
        //foreach ($account->with('balance')->select(['*'])->sortable() as $q) {
        // var_dump($q);
        //}
        return view(
            'account/show-account',
            [
                'account' => $account,
                'balances' => $account->balances()->sortable(['quantity' => 'desc'])->paginate(20),
                'last_use' => $account->getLastUse(),
                'trades' => $trades,
                'errors' => $errors,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        $account->auto_refresh = $request->get('auto_refresh');
        $account->save();
        return redirect()->route('account.show', ['account' => $account]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //Auth::user()->accounts()->delete($account->id);
        $account->forceDelete();
        return redirect()->route('profile');
        //    return redirect()->route('home')->with('success', 'Post deleted.');
    }
}
