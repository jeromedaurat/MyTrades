<?php

namespace App\Http\Requests;

use App\Models\Account;
use Illuminate\Foundation\Http\FormRequest;

class StoreBalance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        var_dump($this->route('account'));
        $account = Account::find($this->route('account'));
        var_dump($account);
        die;
        return $account && $this->user()->can('update', $account);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity' => 'required|float|min:0',
            'account_id' => 'required|unique:accounts',
            'pair_id' => 'required|unique:pairs',
        ];
    }
}
